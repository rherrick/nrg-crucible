# NRG Crucible Docker image

This project builds a Docker image based on the [Atlassian Crucible image](https://hub.docker.com/r/atlassian/crucible),
but with the UID and GID for the **fecru** service user account set to a different value from the default value set in
the base image. By default the UID and GID are set to 2002.

## Building

To build the default image, run the following command from this folder:

```
./build.sh x.y.z
```

Where _x.y.z_ is the version of the new image you want to build and deploy.

This script runs the following command to actually build the image:

```
docker build --tag nrg-crucible:${VERSION} \
             --tag nrg-crucible:latest \
             --tag registry.nrg.wustl.edu/nrg-crucible:${VERSION} \
             --tag registry.nrg.wustl.edu/nrg-crucible:latest .
```

To build the image with different values for the UID and GID, add the following parameters to the `docker build` command:

```
--build-arg=CRUCIBLE_GID=2001 --build-arg=CRUCIBLE_UID=2001
```

Extra build arguments are supported by the `build.sh` script: any parameters after the version number are passed directly
to the `docker build` command:

```
./build.sh x.y.z --build-arg=CRUCIBLE_GID=2001 --build-arg=CRUCIBLE_UID=2001
```

In all cases, you should set the version appropriately, then also tag the new build as **latest**:

```
docker tag nrg-crucible:1.0.0 nrg-crucible:latest
```

## Pushing

Once you've built the image, you can push it to a remote repository. Make sure you include pushing the updated **latest**
tag as well:

```
docker push registry.miskatonic.edu/nrg-crucible:x.y.z
docker push registry.miskatonic.edu/nrg-crucible:latest
```

Note that the `build.sh` script performs this push automatically as long as the `docker build` operation completed succcessfully.

