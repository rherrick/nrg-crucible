FROM atlassian/crucible:latest
MAINTAINER Rick Herrick <jrherrick@wustl.edu>

USER root

ARG CRUCIBLE_GID=2002
ARG CRUCIBLE_UID=2002

RUN groupmod --gid=${CRUCIBLE_GID} fecru; \
    usermod --uid=${CRUCIBLE_UID} fecru; \
    chown -R fecru:fecru /home/fecru; \
    chown -R fecru:fecru /atlassian;

USER fecru

