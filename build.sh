#!/usr/bin/env bash

[[ -z ${1} ]] && { echo "You must provide a version for building and pushing the new image"; exit 1; }

VERSION="${1}"

shift

[[ ${#} == 0 ]] && { REMAINDER="."; } || { REMAINDER="${*} ."; }

# Build and tag image
docker build --tag nrg-crucible:${VERSION} --tag nrg-crucible:latest --tag registry.nrg.wustl.edu/nrg-crucible:${VERSION} --tag registry.nrg.wustl.edu/nrg-crucible:latest ${REMAINDER}

STATUS=${?}
[[ ${STATUS} -gt 0 ]] && { echo "The docker build operation returned a status code of ${STATUS}, which indicates some sort of failure. I won't push any images that may have been built."; exit ${STATUS}; }

echo "Pushing nrg-crucible:${VERSION} to the NRG Docker registry"
docker push registry.nrg.wustl.edu/nrg-crucible:${VERSION}
docker push registry.nrg.wustl.edu/nrg-crucible:latest

